#-*- encoding:UTF-8 -*-

import wx
import data

class MainFrame(wx.Frame):
	
	def __init__(self):
		wx.Frame.__init__(self, None, -1, 'tt', (10,10),(1600, 900))
		self.panel=wx.Panel(self)
		self.Center()
		
		self.menubox = wx.StaticBox(self.panel, wx.ID_ANY, 'menu', (15,10), (100,840))
		self.menulist=[]
		menutip=['select all','select none','back to the precious level','open']
		menulabel=['all','none','cd..','cd']
		menux=20
		menuy=30
		for i in range(len(menulabel)):
			self.menulist.append(wx.Button(self.panel, -1, menulabel[i], (menux,menuy)))
			menuy += 35
			self.menulist[i].SetToolTipString(menutip[i])
		
		self.dirbox = wx.StaticBox(self.panel, wx.ID_ANY, 'file', (120,10), (1210,50))
		self.serachbox = wx.StaticBox(self.panel, wx.ID_ANY, 'search', (1350,10), (220,50))
		self.createshowbox()
	
	def createshowbox(self):
#		self.scroll = wx.ScrolledWindow(self.showbox, -1, (0,15), (1445,760))
#		self.scroll.SetScrollbars(10,10,1600,900)
		
		self.showbox = wx.StaticBox(self.panel, wx.ID_ANY, 'item', (120,70), (1450,780))
		self.itemlist = wx.ListCtrl(self.showbox, wx.ID_ANY, style=wx.LC_REPORT, pos=(0,0), size=(1450,780))
		
#		self.showbox.addscroll()
		
		for i in range(len(d.getitemdata())):
			self.itemlist.InsertColumn(i,d.getitemdata()[i])
			
		for row in range(len(d.getitemname())):
			index = self.itemlist.InsertStringItem(row,d.getitemname()[row],-1)
			for col in range(len(d.getitemdata())):
				self.itemlist.SetStringItem(index,col,d.getsinglevaluebyindex(row,col))
				

def main():
	app=wx.App(False)
	mainframe=MainFrame()
	mainframe.Show()
	app.MainLoop()

if __name__ == '__main__':
	d=data.singledata()
	main()
	
