import urllib2

class MyHttpClient:
    
	@staticmethod
	def generateGetUrl(protocol,baseurl,params):
		base = protocol + '://' + baseurl + '?'
		for key in params.keys():
			base = base + key + '=' + str(params[key]) + '&'
		return base
		
	@staticmethod
	def get_http_request(url):
		print(url)
		fp = urllib2.urlopen(url)
		mybytes = fp.read()
		mystr = bytes.decode(mybytes, 'utf-8')
		fp.close()
		print(mystr)
		return mystr


#MyHttpClient.get_http_request("http://www.bing.com")
