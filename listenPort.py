import threading  
import socket  
  
encoding = 'utf-8'  
BUFSIZE = 10240
status_line = '%s %d %s\r\n' % ("HTTP/1.1", 200, 'OK');
responce = status_line + '''

<html>
%s
</html>'''

# a read thread, read data from remote  
class Reader(threading.Thread):  
    def __init__(self, client, clientaddr,set_code):  
        threading.Thread.__init__(self)  
        self.client = client
        self.clientaddr = clientaddr
        self.set_code = set_code

    def get(self, param_name,data):
        lines = data.split('\r\n')
        strs = lines[0].split(' ');
        dic = self.get_param_dict(strs[1]);
        if(param_name in dic.keys()):
            return dic[param_name]
    
    def run(self):  
        while True:  
            data = self.client.recv(BUFSIZE)
            if(data):  
                string = bytes.decode(data, encoding)  
#                print(string, end='')
                code = self.get('code',string)
                res_str = 'code=' + code;
                print(res_str)
                self.set_code(code)
                self.response(res_str)
            else:  
                break  
        print("close:", self.client.getpeername())  

    def get_param_dict(self,data):
        dic = {}
        param_str = data.split('?')
        if(len(param_str)> 1):
            params = param_str[1].split('&')
            for param in params:
                name_value_pair = param.split('=')
                if(len(name_value_pair) >1):
                    dic[name_value_pair[0]] = name_value_pair[1]
        return dic

    def response(self, returnstr):
        res = responce % str(returnstr)
        self.client.send(res.encode())

# a listen thread, listen remote connect  
# when a remote machine request to connect, it will create a read thread to handle  
class Listener(threading.Thread):  
    def __init__(self, port, set_code):  
        threading.Thread.__init__(self)  
        self.port = port  
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  
        self.sock.bind(("127.0.0.1", port))  
        self.sock.listen(0)
        self.set_code = set_code
    def run(self):  
        print("listener started")  
        client, cltadd = self.sock.accept()  
        Reader(client,cltadd,self.set_code).start()  
        cltadd = cltadd
        print("accept a connect")  

