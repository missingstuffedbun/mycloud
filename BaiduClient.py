import sys
import webbrowser
import time
import json
from MyHttpClient import MyHttpClient
from listenPort import Listener
from listenPort import Reader

import wx
import display

#import cmb
import urllib2

class BaiduClient:
    def __init__(self,appId = 'Xn9QHipr1r1BhlG18tHQy3j4',secretKey = 'QojTxkz0VcD3qiP3FYCtwUZWwlUHDZ0t'):
        self.appId = appId
        self.secretKey = secretKey
        self.redirectUrl = 'http://localhost:10010/oauth_redirect'
        self.code = None
        self.access_token = None
        self.refresh_token = None
        
    def set_code(self,code):
        self.code = code
        print('code set to ' + code)
        
    def getCode(self):
        dictionary = {}
        dictionary['client_id'] = self.appId
        dictionary['response_type'] = 'code'
        dictionary['redirect_uri'] = self.redirectUrl
        #dictionary['redirect_uri'] = 'oob'
        url = MyHttpClient.generateGetUrl('https','openapi.baidu.com/oauth/2.0/authorize',dictionary)
        print(url)
        webbrowser.open(url)
        lst=Listener(10010,self.set_code)
        lst.start()
        print(self.code)

    def getAccessToken(self):
        dictionary = {}
        dictionary['client_id'] = self.appId
        dictionary['client_secret'] = self.secretKey
        dictionary['grant_type'] = 'authorization_code'
        dictionary['redirect_uri'] = self.redirectUrl
        dictionary['code'] = self.code
        url = MyHttpClient.generateGetUrl('https','openapi.baidu.com/oauth/2.0/token',dictionary)
        response = MyHttpClient.get_http_request(url)
        dic = json.loads(response)
        if 'access_token' in dic.keys():
            self.access_token = dic['access_token']
            self.refresh_token = dic['refresh_token']
        return self.access_token

    def __str__(self):
        template = 'client_id : % s \r\nclient_secret : %s \r\naccess_token : %s \r\nrefresh_token : %s\r\n';
        return template % (self.appId , self.secretKey , self.access_token, self.refresh_token)

    def initialize(self):
        self.getCode()
        while not client.code:
            print('no code, waiting')
            time.sleep(1)
        self.getAccessToken()
	
client = BaiduClient()
client.initialize()
print(client)

if __name__=='__main__':
    '''
    access_token='21.6c9e9d3dfb54761d9dce1794f71d9dda.2592000.1432298666.3104052461-5717308'
    path='/app'#PCS API
    urlbasis = 'https://pcs.baidu.com/rest/2.0/pcs/file?'
    url = urlbasis+'method=list&access_token'+access_token+'&path='+path+'&by=name&order=desc'
    responseurl = urllib2.urlopen(url)
    print responseurl.info()
    '''
